import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
    @import url('https://fonts.googleapis.com/css?family=Montserrat');
    * {
        box-sizing: border-box;
    }
    html {
        font-family: 'Montserrat', sans-serif;
    }
`